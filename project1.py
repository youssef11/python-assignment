import cv2
import numpy as np

img = cv2.imread('yoy.jpg')
resimage = cv2.resize(img, (600, 400)) 

imgg = cv2.cvtColor(resimage, cv2.COLOR_BGR2GRAY)


ret,thrshimg = cv2.threshold(resimage,127,255,cv2.THRESH_BINARY)


Z = img.reshape((-1,3))

# convert to np.float32
Z = np.float32(Z)

# define criteria, number of clusters(K) and apply kmeans()
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
K = 8
ret,label,center=cv2.kmeans(Z,K,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)

# Now convert back into uint8, and make original image
center = np.uint8(center)
res = center[label.flatten()]
res2 = res.reshape((img.shape))

cv2.imshow('res2',res2)


cv2.imshow('orginal',img)
cv2.imshow('resized',resimage)
cv2.imshow('gray.Image',imgg)
cv2.imshow('after k-means',thrshimg) 

cv2.waitKey(0)                 
cv2.destroyAllWindows()
